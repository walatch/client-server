# client-server

## **OVERVIEW:**

A Client / Server app based on Python native sockets. 

Uses PostgreSQL or SQLite database for data storage.
 
Server supports user management and basic messaging system.

Server replies in JSON format and accepts plain text input from connected client.

***
**FUNCTIONALITIES:**

Not logged users are able to use following commands:

- `help` - for listing available commands
- `uptime`
- `info`
- `register`
- `login`
- `stop` - to stop client AND the server

Logged-in users - as above - with addition of:

- `logout`
- `changepass` 
- `deleteacc`
- `readinbox` - max inbox size of 6 (unless recipient is "admin")
- `clearinbox`
- `sendmsg`
- `changerights` - only usable if user has "admin" rights

User with "admin" rights usage of most commands is broadened for selecting which user said command should affect.

***

**USAGE:**

Start server.py first - continue as prompted.
After server has started and is awaiting connection - run client.py

Server requires "config.ini" file for proper initialization:

`[SERVER]
HOST = -host-
PORT = -port-
BUFFER = -buffer_size-
ENCODER = utf-8`

`[DATABASE]
HOST = -db_host-
PORT = -db_port-
DATABASE = -db_name-
USER = -db_user-
PASSWORD = -db_user_pass-`

