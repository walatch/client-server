import psycopg2
import sqlite3
from configparser import ConfigParser


def get_config_info_postgresql():
    cp = ConfigParser()
    cp.read("config.ini")
    return (
        cp["POSTREGSQL"]["USER"],
        cp["POSTREGSQL"]["PASSWORD"],
        cp["POSTREGSQL"]["HOST"],
        cp["POSTREGSQL"]["PORT"],
        cp["POSTREGSQL"]["DATABASE"]
    )


def get_config_info_sqlite3():
    cp = ConfigParser()
    cp.read("config.ini")
    return cp["SQLITE3"]["PATH"]


class DB:
    def __init__(self, db_type):
        self.db_type = db_type
        self.q_type = {"postgresql": "%s", "sqlite3": "?"}
        if db_type == "postgresql":
            db_conf = get_config_info_postgresql()
            self.db_conn = psycopg2.connect(
                user=db_conf[0],
                password=db_conf[1],
                host=db_conf[2],
                port=db_conf[3],
                database=db_conf[4])
        if db_type == "sqlite3":
            self.db_conn = sqlite3.connect(r"F:\Pozam\projekty\G2L002\client-server\sqlite3db\client_server.db")

    def create_user(self, user_obj):
        cursor = self.db_conn.cursor()
        # with self.db_conn.cursor() as cursor:
        query = f"""INSERT INTO users (username, password, rights) 
        VALUES ({self.q_type[self.db_type]}, {self.q_type[self.db_type]}, {self.q_type[self.db_type]})"""
        cursor.execute(query, (user_obj.username, user_obj.password, user_obj.rights))
        self.db_conn.commit()
        
    def get_user_data(self, username):
        """ Returns username, password, rights for given username from DB """
        cursor = self.db_conn.cursor()
        # with self.db_conn.cursor() as cursor:
        query = f"SELECT username, password, rights FROM users WHERE username = {self.q_type[self.db_type]}"
        cursor.execute(query, (username,))
        self.db_conn.commit()
        return cursor.fetchone()

    def clear_inbox(self, username):
        """ Clears inbox of a given username in DB """
        cursor = self.db_conn.cursor()
        # with self.db_conn.cursor() as cursor:
        query = f"DELETE FROM inbox WHERE recipient = {self.q_type[self.db_type]}"
        cursor.execute(query, (username,))
        self.db_conn.commit()

    def change_password(self, username, new_password):
        """ Changes password for a given username to new_password in DB"""
        cursor = self.db_conn.cursor()
        # with self.db_conn.cursor() as cursor:
        query = f"""UPDATE users SET password = {self.q_type[self.db_type]} 
        WHERE username = {self.q_type[self.db_type]}"""
        cursor.execute(query, (new_password, username))
        self.db_conn.commit()

    def change_rights(self, username, rights):
        """ Changes rights for a given username in DB"""
        cursor = self.db_conn.cursor()
        # with self.db_conn.cursor() as cursor:
        query = f"""UPDATE users SET rights = {self.q_type[self.db_type]} 
        WHERE username = {self.q_type[self.db_type]}"""
        cursor.execute(query, (rights, username))
        self.db_conn.commit()

    def delete_user(self, username):
        """ Deletes given username from DB and his inbox"""
        self.clear_inbox(username)
        cursor = self.db_conn.cursor()
        # with self.db_conn.cursor() as cursor:
        query = f"DELETE FROM users WHERE username = {self.q_type[self.db_type]}"
        cursor.execute(query, (username,))
        self.db_conn.commit()

    def read_inbox(self, username):
        """ Get all messages of a given username """
        cursor = self.db_conn.cursor()
        # with self.db_conn.cursor() as cursor:
        query = f"""SELECT recipient, sender, message, sent_date 
        FROM inbox WHERE recipient = {self.q_type[self.db_type]} ORDER BY sent_date"""
        cursor.execute(query, (username,))
        return cursor.fetchall()

    def send_msg(self, sender, recipient, message):
        """ Adds given msg to inbox table for a given recipient from given sender """
        cursor = self.db_conn.cursor()
        # with self.db_conn.cursor() as cursor:
        query = f"""INSERT INTO inbox (recipient, sender, message) 
        VALUES ({self.q_type[self.db_type]}, {self.q_type[self.db_type]}, {self.q_type[self.db_type]})"""
        cursor.execute(query, (recipient, sender, message))
        self.db_conn.commit()

    def is_inbox_not_full(self, username):
        """
        Checks if given username inbox is full (max 6) or is 'admin' - if so, returns bool:True
        otherwise bool:False
        """
        user_data = self.get_user_data(username)
        cursor = self.db_conn.cursor()
        # with self.db_conn.cursor() as cursor:
        query = f"""SELECT COUNT(recipient) FROM inbox WHERE recipient = {self.q_type[self.db_type]}"""
        cursor.execute(query, (username,))
        return cursor.fetchone()[0] < 6 or user_data[2] == "admin"


