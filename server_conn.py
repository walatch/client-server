import socket as soc
import json


class Connection:

    def __init__(self, host, port, buffer, encoder):
        self.host = host
        self.port = port
        self.buffer = buffer
        self.encoder = encoder
        self.socket = None
        self.connection = None
        self.address = None

    def __enter__(self):
        # Setting up server socket and connecting with client
        self.socket = soc.socket(soc.AF_INET, soc.SOCK_STREAM)
        self.socket.bind((self.host, self.port))
        self.socket.listen()
        cli_soc, self.address = self.socket.accept()
        self.connection = cli_soc
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()
        del self

    def send_json(self, data):
        # formatting data to JSON
        package = json.dumps(data, indent=2)
        # sending encoded JSON data to CLIENT
        return self.connection.send(package.encode(self.encoder))

    def send_raw(self, data):
        return self.connection.send(data.encode(self.encoder))

    def recv_data(self):
        return self.connection.recv(self.buffer).decode(self.encoder)
