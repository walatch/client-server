import user
from datetime import datetime

__version_info__ = (0, 3, 20210710,)
__version__ = '.'.join(map(str, __version_info__))

# Server info - booted time
srv_booted_time = datetime.now()


def server_info(*args):
    """Returns server version and creation date"""
    srv_ver = f"Server version {__version__}"
    return f"Server booted up at {srv_booted_time} // {srv_ver}"


def uptime(*args):
    """Returns server uptime as a string"""
    srv_uptime = datetime.now() - srv_booted_time
    return f"Server uptime is: {str(srv_uptime)}"


def cli_req_stop(*args):
    return "Closing connection and shutting down server AND client"


def server_cmds_not_logged(*args):
    return server_cmds


def server_cmds_logged_in(*args):
    return server_cmds_logged


server_cmds = {
    "uptime": "Returns uptime of the server",
    "info": "Returns server creation date/time and server version",
    "help": "Returns available commands for a client",
    "stop": "Closes the server and the client",
    "register": "Register new account",
    "login": "Log onto your account"
}

server_cmds_logged = {
    "uptime": "Returns uptime of the server",
    "info": "Returns server creation date/time and server version",
    "help": "Returns available commands for a client",
    "stop": "Closes the server and the client",
    "register": "Register new account",
    "logout": "Log out from your account and saves DB to file",
    "changepass": "Change your password / ADMIN: or others",
    "deleteacc": "Deletes your account / ADMIN: or others",
    "readinbox": "Shows your inbox / ADMIN: or others",
    "clearinbox": "Clears your inbox / ADMIN: or others",
    "sendmsg": "Send a message to a registered friend",
    "changerights": "ADMIN: change users rights"
}


def handler_command_not_found(conn, db_conn):
    return "Invalid command - try typing 'help' for available commands"


def not_logged_handling(conn, db_conn, data, logged_user):
    handlers = {
        "register": user.register,
        "login": user.login,
        "uptime": uptime,
        "info": server_info,
        "help": server_cmds_not_logged,
        "stop": cli_req_stop,
    }

    handler = handlers.get(data, handler_command_not_found)

    if data == "login":
        # login return User object or False
        return handler(conn, db_conn)
    else:
        conn.send_json(handler(conn, db_conn))


def logged_in_handling(conn, db_conn, data, logged_user):
    handlers = {
        "logout": logged_user.logout,
        "register": user.register,
        "changepass": logged_user.change_password,
        "deleteacc": logged_user.delete_account,
        "readinbox": logged_user.read_inbox,
        "clearinbox": logged_user.clear_box,
        "sendmsg": logged_user.send_msg,
        "changerights": logged_user.change_rights,
        "uptime": uptime,
        "info": server_info,
        "help": server_cmds_logged_in,
        "stop": cli_req_stop
    }

    handler = handlers.get(data, handler_command_not_found)
    conn.send_json(handler(conn, db_conn))
