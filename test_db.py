import unittest
import db
from user import User


class TestDB(unittest.TestCase):

    def setUp(self):
        db_conn.db_content = db_conn.load_db()
        # relies on existing admin acc:zxc with at least 6+ msgs
        self.exist_admin = User(*(db_conn.login_data(db_conn.db_content[0]["username"])))
        self.exist_admin_index = db_conn.get_user_data(self.exist_admin.username)
        # relies on existing user acc:poi with >1 and <6 msgs
        self.exist_user = User(*(db_conn.login_data(db_conn.db_content[1]["username"])))
        self.exist_user_index = db_conn.get_user_data(self.exist_user.username)
        # relies on existing user acc:qwerty with 6 msgs to test full inbox
        self.exist_user_full = User(*(db_conn.login_data(db_conn.db_content[2]["username"])))
        self.exist_user_full_index = db_conn.get_user_data(self.exist_user_full.username)
        # creating new user
        self.new_user = User("newuser", "newpass", "user", [])
        db_conn.create_user(self.new_user)
        self.new_user_index = db_conn.get_user_data(self.new_user.username)

    def tearDown(self):
        pass

    def test_append_new_user_to_db(self):
        """Test for adding new user to db"""
        # new user added in set-up // checking if appended correctly
        self.assertEqual(
            db_conn.db_content[-1],
            {"username": self.new_user.username,
             "password": self.new_user.password,
             "rights": self.new_user.rights,
             "inbox": self.new_user.inbox})

    def test_getting_username_db_index(self):
        """Test if given username exists in db and return int:index is
        or bool:False if doesn't exist"""
        self.assertNotEqual(False, db_conn.get_user_data(self.exist_user.username))
        self.assertEqual(False, db_conn.get_user_data("random_name"))

    def test_user_rights_assignment(self):
        """Test for returning user rights"""
        self.assertEqual(db_conn.check_rights(self.exist_user_index), self.exist_user.rights)
        self.assertEqual(db_conn.check_rights(self.exist_admin_index), self.exist_admin.rights)
        self.assertEqual(db_conn.check_rights(self.new_user_index), self.new_user.rights)

    def test_deleting_user_check_db(self):
        """Test for user deletion and checking db if deleted // false if doesn't exist"""
        self.assertTrue(db_conn.delete_user(self.exist_user.username))  # deleting existing user
        self.assertFalse(db_conn.get_user_data(self.exist_user.username))
        self.assertFalse(db_conn.delete_user("random_name"))

    def test_get_inbox_content(self):
        """Test for getting user inbox content"""
        self.assertNotEqual([], db_conn.read_inbox(self.exist_user_index))
        self.assertNotEqual([], db_conn.read_inbox(self.exist_user_full_index))
        self.assertEqual([], db_conn.read_inbox(self.new_user_index))

    def test_send_msg(self):
        msg = "test"
        db_conn.send_msg(self.exist_user_index, self.exist_admin_index, msg)
        self.assertEqual(msg, db_conn.db_content[self.exist_admin_index]["inbox"][-1][1])

    def test_inbox_size(self):
        self.assertTrue(db_conn.is_inbox_not_full(self.exist_user.username))
        self.assertFalse(db_conn.is_inbox_not_full(self.exist_user_full.username))


if __name__ == '__main__':
    unittest.main()
