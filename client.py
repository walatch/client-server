import socket as soc

HOST = "127.0.0.1"
PORT = 9876
BUFFER = 4096
ENCODER = "utf-8"

with soc.socket(soc.AF_INET, soc.SOCK_STREAM) as server:
    server.connect((HOST, PORT))

    while True:
        client_input = input("> ").encode(ENCODER)
        server.sendall(client_input)
        recv_data = server.recv(BUFFER).decode(ENCODER)

        if recv_data == "Closing connection and shutting down server AND client" or not recv_data:
            print(recv_data)
            break

        print(f"Response from server: {recv_data}")
