import server_funcs as funcs
import server_conn as conn
import db

from configparser import ConfigParser

# reading config file for server settings
cp = ConfigParser()
cp.read("config.ini")

# filling config tuples for server
server_config = (
    cp["SERVER"]["HOST"],
    int(cp["SERVER"]["PORT"]),
    int(cp["SERVER"]["BUFFER"]),
    cp["SERVER"]["ENCODER"]
)

if __name__ == "__main__":
    # printing server info to server log
    print(funcs.server_info())
    db_type = input("\nSelect DB type [postgresql , sqlite3]: \n")
    while db_type not in ["postgresql", "sqlite3"]:
        db_type = input("\nSelect DB type [postgresql , sqlite3]: \n")

    # opening connection with client
    with conn.Connection(*server_config) as conn:
        print(f"Connected with client from: {conn.address}")
        # opening connection with DB
        db_conn = db.DB(db_type)
        with db_conn.db_conn:
            logged_in_user = False  # Stores User() object if login is successful
            while True:
                # NOT LOGGED IN USER LOOP
                while not logged_in_user:
                    recv_data = conn.recv_data()
                    if recv_data == "login":
                        # setting logged user obj for login credentials
                        logged_in_user = funcs.not_logged_handling(conn, db_conn, recv_data, logged_in_user)
                    else:
                        funcs.not_logged_handling(conn, db_conn, recv_data, logged_in_user)

                    # breaking out of LOOP for "stop" input from client / server response handled in above handler
                    if recv_data == "stop":
                        break

                # LOGGED IN USER LOOP
                while logged_in_user:
                    recv_data = conn.recv_data()
                    # handling user input
                    funcs.logged_in_handling(conn, db_conn, recv_data, logged_in_user)

                    if recv_data == "logout":
                        logged_in_user = False

                    # breaking out of LOOP for "stop" input from client / server response handled in above handler
                    if recv_data == "stop":
                        break

                if recv_data == "stop":
                    # for "stop" - MAIN LOOP break - shuts down SERVER and CLIENT
                    break
