def cli_get_username(conn, msg="Enter username:"):
    conn.send_json(msg)
    return conn.recv_data().lower()


def cli_get_password(conn, msg="Enter password:"):
    conn.send_json(msg)
    return conn.recv_data()


def cli_get_other(conn, msg="Enter information:"):
    conn.send_json(msg)
    return conn.recv_data()


def register(conn, db_conn):
    """
    Allows connected client to register.
    Creates entry in db from provided username/password
    DOES NOT LOG IN USER
    """
    # Check for empty string or if user already exists
    while True:
        username = cli_get_username(conn)
        if username != "" and db_conn.get_user_data(username) is None:
            break
        else:
            conn.send_json(
                "Username input was empty or such username already exists")
    # Check for empty string or if passwords match
    while True:
        password = cli_get_password(conn)
        confirm_password = cli_get_password(conn, "Confirm password:")
        if password != "" and password == confirm_password:
            break
        conn.send_json("Password input was empty or passwords did not match")
    # creating user
    new_user = User(username, password)
    db_conn.create_user(new_user)  # creates db entry from user object
    del new_user
    return f"Account for {username} has been created"


def login(conn, db_conn):
    """
    Allows connected client to login - checks db for match.
    Returns obj:user if log/pass are correct,
    bool:False if user doesn't exist or log/pass don't match
    """
    username = cli_get_username(conn)
    user_data = db_conn.get_user_data(username)
    if username == "" or user_data is None:
        conn.send_json("Username input was empty or no such username exists")
        return False
    password = cli_get_password(conn)
    if password == user_data[1]:
        user = User(*user_data)
        conn.send_json(f"Login successful - logged in as {username}")
        return user  # returns user object
    conn.send_json("Login failed")
    return False


class User:
    def __init__(self, username, password, rights="user"):
        self.username = username
        self.password = password
        self.rights = rights
        self.inbox = []

    def logout(self, *args):
        """ Display logout message and returns bool:False"""
        del self
        return f"You have been logged out"

    def change_password(self, conn, db_conn):
        """
        Changing password for logged user with credentials check
        and new password check
        ADMIN: choose user to change password
        """
        user_db_data = db_conn.get_user_data(self.username)
        if user_db_data[2] == "user":
            # user rights
            while True:
                password = cli_get_password(conn)
                if password == user_db_data[1]:
                    break
            while True:
                # new password input and check
                new_password = cli_get_password(conn, "Enter your new password:")
                confirm_password = cli_get_password(conn, "Confirm new password:")
                if new_password != "" and new_password == confirm_password:
                    break
            db_conn.change_password(self.username, new_password)
        else:
            # admin rights
            user_for_pwd_change = cli_get_username(conn)
            u_pwd_db_data = db_conn.get_user_data(user_for_pwd_change)
            if user_for_pwd_change == "" or u_pwd_db_data is None:
                conn.send_json("Username input was empty or no such username exists")
                return False
            new_password = cli_get_password(conn, "Enter your new password:")
            confirm_password = cli_get_password(conn, "Confirm new password:")
            while True:
                if new_password != "" and new_password == confirm_password:
                    break
            db_conn.change_password(u_pwd_db_data[0], new_password)
        return "Password change successful"

    def delete_account(self, conn, db_conn):
        """
        Deletes logged-in users account with previous confirmation from user
        ADMIN: choose user for account deletion
        """
        if self.rights == "admin":
            user_to_del = cli_get_username(conn)
            if db_conn.get_user_data(user_to_del) is None or self.username == user_to_del:
                # NO DELETION
                return "No such user trying to delete OWN ADMIN ACCOUNT - are you serious?!"
            db_conn.delete_user(user_to_del)
            # SUCCESSFUL DELETION - ADMIN RIGHTS
            return f"Account for user: -{user_to_del}- has been deleted"
        else:
            response = cli_get_other(conn, "Are you sure you want to delete your account? "
                                           "Type 'DELETE' to continue - anything else to leave)")
            if response == "DELETE":
                db_conn.delete_user(self.username)
                # SUCCESSFUL DELETION
                return f"Your account: {self.username} has been remove from database."
            # NO DELETION
            return "Back to menu - type help for available commands"

    def read_inbox(self, conn, db_conn):
        """
        Displays logged-in user inbox, messages on separate lines
        ADMIN: choose user for inbox display
        """
        username = self.username
        if self.rights == "admin":
            username = cli_get_username(conn)
            if db_conn.get_user_data(username) is None:
                return "No such user."
        return self.inbox_builder(db_conn, username)

    def inbox_builder(self, db_conn, username):
        inbox = db_conn.read_inbox(username)
        if len(inbox) == 0:
            return f"User: {username} has no messages."
        box_to_send = [
            [f"{msg_index}. From user: {i[1]}", f"{i[2]}", f"Sent at: {i[3]}"]
            for msg_index, i in enumerate(inbox, start=1)
        ]
        return [f'{username.capitalize()} has {len(inbox)} messages.', box_to_send]

    def clear_box(self, conn, db_conn):
        """
        Removes all messages from user inbox in DB
        ADMIN: chooser user for inbox CLEANSING!
        """
        if self.rights == "admin":
            user_clr = cli_get_username(conn, "Provide username to for inbox cleansing:")
            user_clr_data = db_conn.get_user_data(user_clr)
            if user_clr_data is False:
                return "No such user."
            # clearing targeted user inbox
            db_conn.clear_inbox(user_clr)
            return f"{user_clr} inbox has been CLEANSED!"
        # clearing user inbox
        db_conn.clear_inbox(self.username)
        return "Your inbox has been cleared."

    def send_msg(self, conn, db_conn):
        """
        Send message to other user

        Prompts user for str:username, checks if user exists or inbox isn't full
        - or type "leave" to back to menu.
        Prompts user for str:message and strips it to 255 characters.
        Appending message to user inbox and save db to file (through db module function).
        """
        while True:
            recipient = cli_get_username(conn)
            user_data = db_conn.get_user_data(recipient)
            if recipient != ""\
                    and user_data is not None \
                    and db_conn.is_inbox_not_full(recipient):
                break
            else:
                return "Recipients inbox is full or no such a recipient"
        msg = cli_get_other(conn, f"Write a message (max 255 characters) to {recipient}:")
        sender = self.username
        db_conn.send_msg(sender, recipient, msg[:256])
        return f"Message has been sent {recipient}."

    def change_rights(self, conn, db_conn):
        """ Allows user with "admin" rights to change other user rights """
        if self.rights == "admin":
            username = cli_get_username(conn)
            user_new_rights = cli_get_other(conn, f"Rights for {username} : ('user' or 'admin') ")
            if user_new_rights in ["user", "admin"] and db_conn.get_user_data(username) is not None:
                db_conn.change_rights(username, user_new_rights)
                return f"{username} rights changed to {user_new_rights}"
            else:
                return f"No such user or unsupported rights"
